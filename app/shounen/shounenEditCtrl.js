(function() {
    "use strict";

    angular.module('shounenApp')
        .controller('ShounenEditCtrl', ShounenEditCtrl);

    function ShounenEditCtrl(shounenItem, $state) {

        var me = this;
        me.shounenItem = shounenItem;

        if (!me.shounenItem.idShounen) {
            me.titulo = "Nuevo Anime";
        } else {
            me.titulo = "Editar: " + me.shounenItem.tituloShounen;
        }
        if(!me.shounenItem.destacado){
            me.shounenItem.destacado = false;
        }

        me.addTags = function (tags){
            var array = tags.split(',');
            me.shounenItem.tags = me.shounenItem.tags? me.shounenItem.tags.concat(array) : array;
            me.shounenTags = "";

        };

        me.removeTag = function(idx){
            me.shounenItem.tags.splice(idx -1, 1);   
        };

        me.showDatepicker = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            me.opened = !me.opened;
        };

        me.guardar = function(){
            me.shounenItem.$save(function (data){
                toastr.success("El anime se ha guardado!!");
            });
        };

        me.cancelar = function(){
            $state.go("shounen");
        };
    }



}());
